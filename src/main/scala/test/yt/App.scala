package test.yt

import akka.actor.{Actor, ActorLogging, ActorRef, ActorSystem, Props}

object TheActor {
  final case class Say(message: String)
  final case class Send(msg: String, sink: ActorRef)
  case object DoThis
  case object DoThat
  def props: Props = Props[TheActor]
}

class TheActor extends Actor with ActorLogging {
  import TheActor._
  import Sink._

  override def receive: Receive = {
    case Say(message) => log.info(message)
    case Send(msg, sink) => sink ! BlackHole(msg + "!")
    case DoThis => log.info("Doing this.")
    case DoThat => log.info("Doing that.")
  }
}

object Sink {
  final case class BlackHole(msg: String)
  def props: Props = Props[Sink]
}

class Sink extends Actor with ActorLogging {
  import Sink._

  override def receive: Receive = {
    case BlackHole(msg) => log.info("Swallowing: " + msg)
  }
}

object MainApp extends App {
  import TheActor._

  val system = ActorSystem("main-app")

  val theActor = system.actorOf(TheActor.props, "the-actor")
  val sink = system.actorOf(Sink.props, "sink")

  theActor ! Say("Hi there!")
  theActor ! DoThis
  theActor ! DoThat
  theActor ! Send("Some text", sink)

//  system.terminate()
}
