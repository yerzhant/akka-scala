package test.yt

import akka.actor.ActorSystem
import akka.testkit.{TestKit, TestProbe}
import org.scalatest.{BeforeAndAfter, Matchers, WordSpecLike}
import test.yt.Sink.BlackHole
import test.yt.TheActor.{DoThat, Send}

class AppSpec() extends TestKit(ActorSystem("AppSpec"))
  with Matchers
  with WordSpecLike
  with BeforeAndAfter {

//  override def afterAll: Unit = {
////    shutdown(system)
//    TestKit.shutdownActorSystem(system)
//  }

  "The Actor" should {
    "do what it is said to do" in {
      val testProbe = TestProbe()
      val theActor = system.actorOf(TheActor.props)
      theActor ! DoThat
      theActor ! Send("Test msg", testProbe.ref)
      testProbe.expectMsg(BlackHole("Test msg!"))
    }
  }
}
